/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      backgroundImage: {
        earth: "url('./src/assets/images/home/img-earth.png)",
      },
      colors: {
        primary: "#f7630c",
      },
      backgroundColor: {
        primary: "#f7630c",
        gradient: "red",
      },
    },
  },
  plugins: [],
};
