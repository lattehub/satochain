import clsx from "clsx";

export default function CommonButton(props: {
  text?: string;
  className?: string;
  prefix?: any;
  suffix?: any;
  onClick?: any;
}) {
  return (
    <div
      onClick={props.onClick}
      className={clsx([
        props.className && props.className,
        "flex items-center gap-x-2 py-2 px-6 rounded-full cursor-pointer hover:opacity-70",
      ])}
    >
      {props.prefix && props.prefix}
      {props.text && <span className="text-center flex-1">{props.text}</span>}
      {props.suffix && props.suffix}
    </div>
  );
}
