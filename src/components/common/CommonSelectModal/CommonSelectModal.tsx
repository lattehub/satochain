import icArrowDown from "assets/icons/ic-arrow-down.svg";
import sBtcIc from "assets/icons/ic-bitcoin-2.png";
import satoIc from "assets/icons/ic-logo.png";
import icSearch from "assets/icons/ic-search.png";
import { useState } from "react";
import CommonInput from "../CommonInput/CommonInput";
import "./selectModal.css";
export default function CommonSelectModal(props: { className?: string }) {
  const [isShowModal, setIsShowModal] = useState(false);
  const [searchText, setSearchText] = useState("");
  const options = [
    {
      text: "SATO",
      icon: satoIc,
    },
    {
      text: "sBTC",
      icon: sBtcIc,
    },
  ];

  return (
    <div>
      {/* Modal toggle */}
      <div
        className="rounded-md flex items-center gap-x-4 cursor-pointer border p-[8px] border-white/15 relative z-0"
        onClick={() => {
          setIsShowModal(true);
        }}
      >
        <img className="w-[24px] object-contain" src={sBtcIc} />
        <span>sBTC</span>
        <img src={icArrowDown} />
      </div>
      {/* Main modal */}
      <div
        className={`overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full flex md:inset-0 max-h-full modal-overlay ${
          isShowModal ? "block" : "hidden"
        }`}
      >
        <div className="relative p-8 w-full max-w-2xl max-h-full m-auto z-[1000]">
          {/* Modal content  */}
          <div className="relative rounded-lg shadow z-[1000] modal">
            {/* Modal header */}
            <div className="flex items-center justify-between p-4 md:p-5 rounded-t">
              <h3 className="text-xl text-white text-[36px] font-black">
                Select a token
              </h3>
              <button
                type="button"
                style={{ zIndex: 1000 }}
                className="text-gray-400 hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white relative z-50"
                onClick={() => {
                  setIsShowModal(false);
                }}
              >
                <svg
                  className="w-3 h-3"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 14 14"
                >
                  <path
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
                  />
                </svg>
                <span className="sr-only">Close modal</span>
              </button>
            </div>
            {/* Modal body */}
            <div className="p-4 md:p-5 space-y-4">
              <div className="border-b pb-[30px] border-b-white/15">
                <CommonInput
                  prefix={<img className="ml-1" src={icSearch} />}
                  label="Search"
                  setValue={setSearchText}
                  value={searchText}
                  bgColorInput="modal-search-bg"
                />
              </div>
              <div>
                {options.map((o, number) => {
                  return (
                    <div
                      key={number}
                      className="flex items-center gap-x-2 mb-3 p-3 cursor-pointer hover:bg-[#f7630c14] rounded-lg"
                    >
                      <img src={o.icon} className="w-[32px] object-contain" />
                      <span>{o.text}</span>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
