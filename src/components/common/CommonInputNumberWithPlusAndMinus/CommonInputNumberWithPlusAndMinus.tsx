import { ReactNode } from "react";

export default function CommonInputNumberWithPlusAndMinus(props: {
  labelElement?: ReactNode;
  label?: string;
  value: number;
  setValue: any;
}) {
  return (
    <form className="max-w-xs mx-auto relative z-50">
      {props.label && (
        <label
          htmlFor="quantity-input"
          className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
        >
          {props.label}
        </label>
      )}
      {props.labelElement && props.labelElement}
      <div className="relative flex items-center max-w-[150px] border-[1px] border-white/10 rounded-xl p-1">
        <button
          type="button"
          id="decrement-button"
          data-input-counter-decrement="quantity-input"
          className=" rounded-s-lg p-3 bg-white/10 text-white"
          onClick={() => {
            props.setValue((prev: number) => prev - 1);
          }}
        >
          <svg
            className="w-3 h-3 text-white"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 18 2"
          >
            <path
              stroke="currentColor"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M1 1h16"
            />
          </svg>
        </button>
        <input
          value={props.value}
          type="text"
          id="quantity-input"
          data-input-counter
          aria-describedby="helper-text-explanation"
          className="w-full p-2.5 text-sm bg-transparent text-center"
          placeholder="999"
          required
          onChange={(e) => {
            props.setValue(Number(e.target.value));
          }}
        />
        <button
          type="button"
          id="increment-button"
          data-input-counter-increment="quantity-input"
          className="rounded-e-lg p-3 bg-white/10 text-white"
          onClick={() => {
            console.log(123123);
            props.setValue((prev: number) => prev + 1);
          }}
        >
          <svg
            className="w-3 h-3 text-white"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 18 18"
          >
            <path
              stroke="currentColor"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M9 1v16M1 9h16"
            />
          </svg>
        </button>
      </div>
    </form>
  );
}
