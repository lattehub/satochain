import clsx from "clsx";
import { ReactNode } from "react";

export default function CommonInput(props: {
  label: string;
  className?: string;
  placeHolder?: string;
  suffix?: ReactNode;
  prefix?: ReactNode;
  value: string;
  setValue: (value: string) => void;
  bgColorInput?: string;
}) {
  return (
    <div className={clsx(["relative", props.className && props.className])}>
      <input
        type="text"
        id={props.label}
        className={`block px-2.5 pb-2.5 pt-4 w-full text-sm text-[#F1DFD9] bg-black rounded-lg border border-gray-300 appearance-none focus:outline-none focus:ring-0 peer focus:border-[#F1DFD9] focus:border-[2px] ${
          props.prefix && "pl-[25px]"
        } ${props.bgColorInput ? props.bgColorInput : "bg-black"}`}
        placeholder={props.placeHolder ?? props.label}
        value={props.value}
        onChange={(e) => {
          props.setValue(e.target.value);
        }}
      />
      <label
        htmlFor={props.label}
        className={`absolute text-sm text-[#D8C2BA] dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] dark:bg-gray-900 px-2 peer-focus:px-2  peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto start-1 pl-5 bg-black py-[1px] w-fit  ${
          props.bgColorInput ? props.bgColorInput : "bg-black"
        }`}
      >
        {props.label}
      </label>
      {props.prefix && (
        <div className="absolute left-0 top-[13px] hover:opacity-70 cursor-pointer">
          {props.prefix}
        </div>
      )}
      {props.suffix && (
        <div className="absolute right-0 top-0 hover:opacity-70 cursor-pointer">
          {props.suffix}
        </div>
      )}
    </div>
  );
}
