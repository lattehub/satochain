import { ReactNode } from "react";
import "./select.css";
export default function CommonSelect(props: {
  label?: string;
  options: {
    icon?: ReactNode;
    label: string;
    value: number;
  }[];
}) {
  return (
    <form className="max-w-sm mx-auto relative z-50">
      {props.label && (
        <label
          htmlFor="options"
          className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
        >
          Select an option
        </label>
      )}
      <select
        id="options"
        className="w-[166px] bg-transparent border-[1px] border-[#A08D86] text-white text-sm rounded-lg block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
      >
        {props.options.map((option) => (
          <option key={option.value} value={option.value}>
            {option.icon && option.icon} {option.label}
          </option>
        ))}
      </select>
    </form>
  );
}
