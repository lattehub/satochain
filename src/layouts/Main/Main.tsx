import React, { Suspense } from "react";
import Header from "../Header/Header";
import { Route, Routes } from "react-router-dom";
import CommonLoading from "components/common/CommonLoading/CommonLoading";

export default function Main() {
  const HomePage = React.lazy(() => import("views/home/HomePage"));
  const MintKeyPage = React.lazy(() => import("views/mintKey/MintKeyPage"));
  const FaucetPage = React.lazy(() => import("views/faucet/FaucetPage"));
  const SwapPage = React.lazy(() => import("views/swap/SwapPage"));
  return (
    <div className="bg-black min-h-screen">
      <Header />
      <Routes>
        <Route
          path=""
          element={
            <Suspense fallback={<CommonLoading />}>
              <HomePage></HomePage>
            </Suspense>
          }
        ></Route>
        <Route
          path="mintkey"
          element={
            <Suspense fallback={<CommonLoading />}>
              <MintKeyPage />
            </Suspense>
          }
        ></Route>
        <Route
          path="faucet"
          element={
            <Suspense fallback={<CommonLoading />}>
              <FaucetPage></FaucetPage>
            </Suspense>
          }
        ></Route>
        <Route
          path="swap"
          element={
            <Suspense fallback={<CommonLoading />}>
              <SwapPage></SwapPage>
            </Suspense>
          }
        ></Route>
      </Routes>
    </div>
  );
}
