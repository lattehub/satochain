import icLogo from "assets/icons/ic-logo.png";
import icWallet from "assets/icons/ic-wallet.png";
import logo from "assets/logo/Sotachain.png";
import CommonButton from "components/common/CommonButton/CommonButton";
import { Navigation } from "models/layouts/Header/HeaderModel";
import { useState } from "react";
import { NavLink } from "react-router-dom";
import "./header.css";
export default function Header() {
  const [isShowMenuSmallScreen, setIsShowMenuSmallScreen] = useState(false);
  const navigations: Navigation[] = [
    {
      text: "Home",
      routerLink: "",
    },
    {
      text: "Mint Key",
      routerLink: "/mintkey",
      isHot: true,
    },
    {
      text: "Faucet",
      routerLink: "/faucet",
    },
    {
      text: "Swap",
      routerLink: "/swap",
    },
  ];
  const comingSoon = [
    {
      text: "Explorer",
    },
    {
      text: "Bridge",
    },
  ];
  return (
    <div className="container-page w-full pt-[28.5px] pb-[17.5px] flex items-center justify-between z-30 ">
      <div className="flex items-center gap-x-2">
        <img src={logo} />
        <span className="text-xl font-bold text-white">Satochain</span>
      </div>
      {navigations.map((nav: Navigation, index: number) => {
        return (
          <div key={index} className="relative hidden xl:block">
            <NavLink
              to={nav.routerLink!}
              className={({ isActive }) =>
                isActive ? "color-linear" : "text-white"
              }
            >
              {nav.text}
              {nav.isHot && (
                <span className="absolute top-[-20px] right-[-30px] text-white bg-linear p-1 rounded-lg uppercase font-medium text-[11px]">
                  hot
                </span>
              )}
            </NavLink>
          </div>
        );
      })}
      {comingSoon.map((nav: Navigation, index: number) => {
        return (
          <div key={index} className="relative text-gray-500 xl:block hidden">
            {nav.text}
            <span className="absolute top-[-20px] right-[-30px] text-gray-500 uppercase text-[11px]">
              soon
            </span>
          </div>
        );
      })}
      <div className="hidden xl:flex items-center gap-x-3">
        <CommonButton
          text="Connect UniSat"
          className="bg-linear"
          prefix={<img src={icLogo} />}
        ></CommonButton>
        <CommonButton
          text="Connect EVM"
          className="bg-linear"
          prefix={<img src={icWallet} />}
        ></CommonButton>
      </div>
      <div className="relative">
        <div
          onClick={() => {
            setIsShowMenuSmallScreen((prev) => !prev);
          }}
          className="text-white block xl:hidden hover:bg-white cursor-pointer hover:opacity-70 p-2 rounded-lg hover:text-black px-6 z-50 relative"
        >
          <i className="fa-solid fa-bars"></i>
        </div>
        {isShowMenuSmallScreen && (
          <div className="absolute bg-[#3b3a3a] text-white p-2 rounded-lg right-0 flex flex-col gap-y-3 w-[300px] z-[1000] top-14">
            {navigations.map((nav: Navigation, index: number) => {
              return (
                <div
                  key={index}
                  className="p-2 bg-black hover:bg-linear hover:cursor-pointer rounded-md hover:bg-primary hover:font-semibold"
                >
                  <NavLink
                    to={nav.routerLink!}
                    className={({ isActive }) =>
                      isActive ? "color-linear" : "text-white"
                    }
                    onClick={() => {
                      setIsShowMenuSmallScreen(false);
                    }}
                  >
                    {nav.text}
                    {nav.isHot && (
                      <span className="text-white bg-linear p-1 rounded-lg uppercase font-medium text-[11px] ml-5">
                        hot
                      </span>
                    )}
                  </NavLink>
                </div>
              );
            })}
            {comingSoon.map((nav: Navigation, index: number) => {
              return (
                <NavLink
                  key={index}
                  to={nav.routerLink!}
                  onClick={() => {
                    setIsShowMenuSmallScreen(false);
                  }}
                  className={
                    "text-gray-500 p-2 bg-black hover:bg-linear hover:cursor-pointer  rounded-md"
                  }
                >
                  {nav.text}
                  <span className="text-gray-500 uppercase text-[11px] ml-5">
                    soon
                  </span>
                </NavLink>
              );
            })}
            <div className="flex items-center gap-x-2 w-full justify-center">
              <CommonButton
                text="Connect UniSat"
                className="bg-linear flex-1 text-xs"
              ></CommonButton>
              <CommonButton
                text="Connect EVM"
                className="bg-linear flex-1 text-xs"
              ></CommonButton>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
