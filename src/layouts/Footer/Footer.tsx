import imgEarth from "assets/images/footer/Earth.png";
import imgBlock from "assets/images/footer/Block.png";
import iconX from "assets/icons/ic-x.png";
import iconDiscord from "assets/icons/ic-discord.png";
import logoWhite from "assets/logo/logo-white.png";
import CommonButton from "components/common/CommonButton/CommonButton";
import "./footer.css";
export default function Footer(props: { className?: string }) {
  return (
    <div className="w-full flex justify-center">
      <div
        className={`w-full flex-1 xl:w-[1000px] 2xl:w-[1300px] text-white ${props.className} bg-primary pt-[20px] lg:pt-[50px] xl:pt-[70px] rounded-[48px] pb-[30px] px-3`}
      >
        <div className="flex justify-between xl:flex-row flex-col items-center">
          <img className="footer-image" src={imgEarth} />
          <div className="flex flex-col items-center">
            <div className="flex items-center gap-x-4 mb-8">
              <img
                src={logoWhite}
                className="sm:w-[39px] object-contain w-[25px]"
              />
              <span className="text-xl sm:text-[28px] font-bold">
                Satochain
              </span>
            </div>
            <div className="text-center mb-[40px]">
              <div className="sm:mb-6 text-xl sm:text-2xl leading-10 lg:text-[45px] font-black">
                Make Bitcoin mass adopt
              </div>
              <div className="text-sm sm:text-base">
                Engage, collaborate, and connect with thousands of Satochainer
                globally.
              </div>
            </div>
          </div>
          <img className="footer-image" src={imgBlock} />
        </div>
        <div className="flex items-center justify-center gap-x-4 mt-5 lg:mt-0 mb-[40px] sm:mb-[100px]">
          <CommonButton
            className="pt-1 pb-1 pl-1 pr-4 bg-white/10 hover:bg-white/50"
            text="Twitter"
            prefix={<img src={iconX} />}
          />
          <CommonButton
            className="pt-1 pb-1 pr-1 pl-4 bg-white/10 hover:bg-white/50"
            text="Discord"
            suffix={<img src={iconDiscord} />}
          />
        </div>
        <div className="pt-8 px-4 sm:px-[60px] flex flex-col sm:flex-row items-start  sm:items-center xl:justify-between flex-wrap sm:justify-center gap-y-2 xl:gap-y-0  border-white/10 border-t-[0.5px] text-sm sm:text-base">
          <div>© 2024 Satochain. All right reserved.</div>
          <div className="flex gap-x-[36px] flex-col sm:flex-row">
            <div>Privacy policy</div>
            <div>Explorer</div>
            <div>Satochain Bounty Program</div>
          </div>
        </div>
      </div>
    </div>
  );
}
