import icDiscord from "assets/icons/ic-discord-dark.svg";
import icX from "assets/icons/ic-x-dark.svg";
// import "styles/views/mintkey/mintkey.css";
export default function Footer2() {
  return (
    <div className="border-t-[0.5px] border-white/10">
      <div className="py-[40px] flex items-center justify-between container-page">
        <span className="text-white/75 text-xs sm:text-sm">
          © 2024 Satochain. All right reserved.
        </span>
        <div className="flex items-center gap-x-4">
          <div className="p-[10px] rounded-full bg-white/10">
            <img src={icX} className="w-[10px] sm:w-[20px] object-contain" />
          </div>
          <div className="p-[10px] rounded-full bg-white/10">
            <img
              src={icDiscord}
              className="w-[10px] sm:w-[20px] object-contain"
            />
          </div>
        </div>
      </div>
    </div>
  );
}
