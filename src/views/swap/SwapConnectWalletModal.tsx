import fox1 from "assets/images/swap/fox-1.svg";
import fox2 from "assets/images/swap/fox-2.svg";
import coinbase1 from "assets/images/swap/coinbase-1.svg";
import coinbase2 from "assets/images/swap/coinbase-2.png";
import rainbow1 from "assets/images/swap/rainbow-1.svg";
import rainbow2 from "assets/images/swap/rainbow-2.png";
import wallet1 from "assets/images/swap/wallet-connect-1.svg";
import wallet2 from "assets/images/swap/wallet-2.png";
import "./swapWalletModal.css";
export default function SwapConnectWalletModal(props: {
  isShow: boolean;
  onClose: any;
}) {
  return (
    <div
      className={`overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full flex md:inset-0 max-h-full modal-overlay ${
        props.isShow ? "block" : "hidden"
      }`}
    >
      <div className="p-8 w-full max-w-2xl max-h-full m-auto z-[1000]">
        {/* Modal content  */}
        <div className="rounded-lg shadow z-[1000] swap-connect-wallet-modal">
          {/* Modal header */}
          <div className="flex items-center justify-between swap-connect-wallet-padding pt-[32px] mb-[40px] rounded-t">
            <h3 className="text-xl text-white text-[36px] font-black">
              Connect Wallet
            </h3>
            <button
              type="button"
              style={{ zIndex: 1000 }}
              className="text-gray-400 hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white relative z-50"
              onClick={props.onClose}
            >
              <svg
                className="w-3 h-3"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 14 14"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
                />
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
          </div>
          {/* Modal body */}
          <div className="swap-connect-wallet-padding flex flex-col gap-y-4 pb-8">
            <div
              className={`bg-gradient-to-r from-[#FF8500] to-[#FF6A00] swap-connect-btn flex items-center justify-between`}
            >
              <div className="flex items-center gap-x-2">
                <img src={fox1} />
                <span>MetaMask</span>
              </div>
              <img src={fox2} />
            </div>
            <div
              className={`bg-gradient-to-r from-[#548BFF] to-[#1560FF] swap-connect-btn flex items-center justify-between`}
            >
              <div className="flex items-center gap-x-2">
                <img src={coinbase1} />
                <span>Coinbase wallet</span>
              </div>
              <img src={coinbase2} />
            </div>
            <div
              className={`bg-gradient-to-r from-[#2C59B2] to-[#164196] swap-connect-btn flex items-center justify-between`}
            >
              <div className="flex items-center gap-x-2">
                <img src={rainbow1} />
                <span>Rainbow wallet</span>
              </div>
              <img src={rainbow2} />
            </div>
            <div
              className={`bg-gradient-to-r from-[#67B1FF] to-[#3396FF] swap-connect-btn flex items-center justify-between`}
            >
              <div className="flex items-center gap-x-2">
                <img src={wallet1} />
                <span>WalletConnect</span>
              </div>
              <img src={wallet2} />
            </div>
            <div className="font-normal">
              By connecting a wallet, you agree to{" "}
              <span className="swap-connect-wallet-color-gradient">
                Terms of Service
              </span>{" "}
              and{" "}
              <span className="swap-connect-wallet-color-gradient">
                Privacy Policy.
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
