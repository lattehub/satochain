import icCog from "assets/icons/ic-cog.png";
import imgGradientLeft from "assets/images/mintkey/img-gradient-left.svg";
import imgGradientRight from "assets/images/mintkey/img-gradient-right.png";
import CommonSelectModal from "components/common/CommonSelectModal/CommonSelectModal";
import Footer from "layouts/Footer/Footer";
import "./swap.css";
import "./swapWalletModal.css";
import icSwap from "assets/icons/ic-swap.svg";
import CommonButton from "components/common/CommonButton/CommonButton";
import mark from "assets/icons/ic-exclamation-mark.png";
import CommonInput from "components/common/CommonInput/CommonInput";
import { useState } from "react";
import SwapConnectWalletModal from "views/swap/SwapConnectWalletModal";
export default function SwapPage() {
  const [isShowOption, setIsShowOption] = useState(false);
  const [isShowWalletConnect, setIsShowWalletConnect] = useState(false);
  return (
    <div className="relative pt-[40px] sm:pt-[60px] md:pt-[155px]">
      <img
        src={imgGradientLeft}
        className="w-[200px] xl:w-[383px] absolute left-0 z-10 top-[-140px] object-contain"
      />
      <img
        src={imgGradientRight}
        className="absolute right-0 z-10 top-[-100px] 2xl:w-[500px] w-[200px] xl:block hidden object-contain"
      />
      <div className="mb-[100px] text-white container-page">
        <div className="mx-auto w-auto lg:w-[627px] swap-card p-[24px] sm:p-[40px]">
          <div className="flex items-center justify-between ">
            <div className="text-[24px] sm:text-[45px] font-black">Swap</div>
            <div className="relative">
              <img
                onClick={() => {
                  setIsShowOption((prev) => !prev);
                }}
                className="hover:cursor-pointer object-contain "
                src={icCog}
              />
              {/* NOTE: options */}
              {isShowOption && (
                <div className="absolute right-0 w-[300px] sm:w-[466px] swap-option p-[20px] flex flex-col gap-y-5">
                  <div>
                    <div className="flex items-center gap-x-2">
                      <span>Slippage tolerance</span>
                      <img src={mark} />
                    </div>
                    <div className="swap-option-select">
                      <div className="flex border border-[#F1DFD9] rounded-full w-full overflow-hidden">
                        <div className="w-1/3 text-center border-r border-[#F1DFD9] py-2 px-3 hover:bg-[#F1DFD9]/50 hover:text-[#F1DFD9] cursor-pointer">
                          Auto
                        </div>
                        <div className="w-1/3 text-center py-2 px-3 border-r hover:bg-[#F1DFD9]/50 hover:text-[#F1DFD9] border-[#F1DFD9] cursor-pointer">
                          Custom
                        </div>
                        <div className="w-1/3 text-center py-2 px-3 hover:bg-[#F1DFD9]/50 hover:text-[#F1DFD9] cursor-pointer">
                          Degen
                        </div>
                      </div>
                      <CommonInput
                        value=""
                        setValue={() => {}}
                        label="1%"
                        bgColorInput="swap-input-color"
                      />
                    </div>
                  </div>
                  <div>
                    <div className="flex items-center gap-x-2">
                      <span>Slippage tolerance</span>
                      <img src={mark} />
                    </div>
                    <div className="swap-option-select">
                      <div className="flex border border-[#F1DFD9] rounded-full w-full overflow-hidden">
                        <div className="w-1/3 text-center border-r border-[#F1DFD9] py-2 px-3 hover:bg-[#F1DFD9]/50 hover:text-[#F1DFD9] cursor-pointer">
                          Auto
                        </div>
                        <div className="w-1/3 text-center py-2 px-3 border-r border-[#F1DFD9] hover:bg-[#F1DFD9]/50 hover:text-[#F1DFD9] cursor-pointer">
                          Custom
                        </div>
                        <div className="w-1/3 text-center py-2 px-3 hover:bg-[#F1DFD9]/50 hover:text-[#F1DFD9] cursor-pointer">
                          Degen
                        </div>
                      </div>
                      <CommonInput
                        value=""
                        setValue={() => {}}
                        label="30min"
                        bgColorInput="swap-input-color"
                      />
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
          <div className="mt-[24px]">
            <div className="mb-1 text-[#888]">From</div>
            <div className="swap-input-select flex items-center justify-between">
              <CommonSelectModal />
              <span className="text-[28px] font-extrabold cursor-pointer hover:text-primary">
                0
              </span>
            </div>
          </div>
          <div className="flex justify-center w-full mt-[24px] mb-[8px]">
            <div className="border border-white/15 rounded-[4px] bg-white/5 w-fit p-2 hover:border-primary cursor-pointer">
              <img className="mx-auto" src={icSwap} />
            </div>
          </div>
          <div className="mt-[24px]">
            <div className="mb-1 text-[#888]">To</div>
            <div className="swap-input-select flex items-center justify-between relative z-0">
              <CommonSelectModal />
              <span className="text-[28px] font-extrabold cursor-pointer hover:text-primary">
                0
              </span>
            </div>
          </div>
          <div className="text-[14px] m:text-base mt-[40px] rounded-md bg-white/10 py-2 px-4 text-white/45">
            <div className="flex items-center justify-between ">
              <span>Exchange rate</span>
              <span className="text-white">-</span>
            </div>
            <div className="flex items-center justify-between">
              <span>Gas fee</span>
              <span className="text-white mt-2">0.00001 sBTC</span>
            </div>
          </div>
          <CommonButton
            onClick={() => {
              setIsShowWalletConnect(true);
            }}
            text="Connect"
            className="bg-primary mt-[40px]"
          />
          <SwapConnectWalletModal
            isShow={isShowWalletConnect}
            onClose={() => {
              setIsShowWalletConnect(false);
            }}
          />
        </div>
      </div>
      <div className="container-page">
        <Footer className="mb-20" />
      </div>
    </div>
  );
}
