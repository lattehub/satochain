import icBitcoin from "assets/icons/ic-bitcoin.png";
import icClose from "assets/icons/ic-close.png";
import imgAtm from "assets/images/faucet/img-amt-bitcoin.png";
import imgGradientLeft from "assets/images/mintkey/img-gradient-left.svg";
import imgGradientRight from "assets/images/mintkey/img-gradient-right.png";
import CommonButton from "components/common/CommonButton/CommonButton";
import CommonInput from "components/common/CommonInput/CommonInput";
import Footer from "layouts/Footer/Footer";
import { useState } from "react";
export default function FaucetPage() {
  const [walletAddress, setWalletAddress] = useState("");
  return (
    <div className="relative">
      <img
        src={imgGradientLeft}
        className="absolute left-0 z-10 top-[-140px] object-contain"
      />
      <img
        src={imgGradientRight}
        className="absolute right-0 z-10 top-[-100px] w-[300px] xl:w-[769px]  object-contain"
      />
      <div className="w-fit xl:w-[1300px] flex-col-reverse gap-y-10 xl:gap-y-0 justify-center xl:flex-row flex items-center xl:justify-between text-white mx-auto mt-10 mb-14 sm:mb-36 container-page">
        <div className="flex-1 xl:w-[481px]">
          <div className="text-[30px] sm:text-[45px] font-black">
            Bootstrap Your Testnet Wallet
          </div>
          <div className="flex items-center gap-x-2 text-[#ffffffd1] text-base sm:text-[20px] mt-[25px]">
            <span>Fund your testnet wallet with</span>
            <img src={icBitcoin} />
            <span>sBTC</span>
          </div>
          <CommonInput
            value={walletAddress}
            setValue={setWalletAddress}
            label="Wallet Address * "
            placeHolder="Fill..."
            className="mt-[41px]"
            suffix={
              <img
                onClick={() => {
                  setWalletAddress("");
                }}
                src={icClose}
              />
            }
          />
          <CommonButton
            text="Confirm"
            className="mt-[48px] bg-primary xl:w-full w-fit mx-auto"
          />
          <div className="mt-[40px] sm:mt-[80px] font-normal text-white/75">
            To ensure a sufficient balance for all users, the Faucet is set to
            dispense 0.001 testnet sBTC tokens for each wallet
          </div>
        </div>
        <div>
          <img src={imgAtm} className="lg:w-[587px] w-[400px]" />
        </div>
      </div>
      <div className="-page">
        <Footer className="mb-20" />
      </div>
    </div>
  );
}
