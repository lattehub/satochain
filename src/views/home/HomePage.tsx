import iconArrowRight from "assets/icons/ic-arrow-right.png";
import iconCheck from "assets/icons/ic-vector.png";
import imgGradient from "assets/images/home/image-gradient.svg";
import imgLock from "assets/images/home/image-lock.png";
import imgBag from "assets/images/home/img-bag.png";
import imgComputer from "assets/images/home/img-computer.png";
import imgDefi from "assets/images/home/img-defi.svg";
import imgGradientBag from "assets/images/home/img-gradient-bag.png";
import imgGradientComputer from "assets/images/home/img-gradient-computer.png";
import imgGradientLock from "assets/images/home/img-gradient-lock.png";
import imgNft from "assets/images/home/img-nft.svg";
import imgOrange from "assets/images/home/img-orange.png";
import imgRocket from "assets/images/home/img-rocket.png";
import imgSmartContract from "assets/images/home/img-smartcontract.svg";
import CommonButton from "components/common/CommonButton/CommonButton";
import Footer from "layouts/Footer/Footer";
import { useNavigate } from "react-router-dom";
import "./home.css";
interface Feature {
  img: string;
  text: string;
  description: string;
  list?: string[];
}
export default function HomePage() {
  const navigate = useNavigate();
  const features: Feature[] = [
    {
      img: imgSmartContract,
      text: "Smart Contract",
      description:
        "The consensus mechanism linking Satochain with Bitcoin, enabling applications to utilize Bitcoin's security.",
    },
    {
      img: imgDefi,
      text: "Bitcoin DeFi",
      description:
        "Satochain brings DeFi to Bitcoin, unlocking over $300 billion in capital and setting the stage for activation of the Bitcoin economy.",
    },
    {
      img: imgNft,
      text: "Bitcoin NFTs",
      description:
        "Satochain launches DeFi on Bitcoin, releasing over $300 billion in funds and catalyzing the Bitcoin economy's growth",
    },
  ];
  const goToMintKeyPage = () => {
    navigate("/mintkey");
  };
  return (
    <div className="text-white py-10 relative">
      <img className="absolute right-0 z-10 top-[-100px]" src={imgOrange} />
      <img className="absolute left-0 z-10 top-[-100px]" src={imgGradient} />
      <div className="flex flex-col lg:flex-row items-center lg:items-start justify-between px-7 md:px-[118px]">
        <div className="py-10">
          <div className="text-3xl lg:text-[50px] xl:text-[64px] font-black">
            Build for Bitcoin
          </div>
          <div className="pt-7 font-normal lg:text-lg xl:text-xl opacity-85 mb-10">
            Enhance Bitcoin's economy using secure, Bitcoin-based apps and smart
            contracts.
          </div>
          <CommonButton
            onClick={goToMintKeyPage}
            text="Mint Entry Key"
            className="bg-primary w-fit py-2"
            suffix={<img src={iconArrowRight} />}
          />
        </div>
      </div>
      <div className="relative bg-earth h-[720px] sm:h-[1000px] md:h-[1200px] lg:h-[1500px] xl:h-[2300px] xl:mt-72 z-10 flex flex-col items-center justify-center">
        {/* <div className="blend"></div>
        <img src={imgEarth} /> */}
        <img
          className="z-20 w-[150px] sm:w-[250px] md:w-[300px] lg:w-[400px] xl:w-[638px] absolute top-[-10px] sm:top-[50px] md:top-[65px] right-[30px] lg:right-[-10px] lg:top-[10px] xl:right-[150px] xl:top-[-100px]"
          src={imgRocket}
        />
        <div className="font-black text-2xl sm:text-3xl lg:text-4xl xl:text-5xl">
          Features
        </div>
        <div className="flex gap-x-1 lg:gap-x-10 mt-4 sm:mt-16">
          {features.map((f: Feature, index) => {
            return (
              <div
                key={index}
                className="w-[120px] sm:w-[180px] xl:w-[300px] 2xl:w-[400px] flex flex-col items-center"
              >
                <img
                  src={f.img}
                  alt={f.text}
                  className="w-[60px] sm:w-[80px] md:w-[150px] lg:w-[200px] object-contain"
                />
                <div className="color-linear text-sm w-[60px] sm:w-fit sm:text-lg lg:text-3xl font-extrabold mb-6 mt-3 text-center">
                  {f.text}
                </div>
                <div className="text-center text-sm lg:text-base leading-6 hidden md:block">
                  {f.description}
                </div>
              </div>
            );
          })}
        </div>
      </div>
      <div className="flex flex-col gap-y-24">
        <div className="flex flex-col items-center lg:flex-row lg:items-start lg:justify-evenly relative">
          <img src={imgLock} className="home-image-feature" />
          <img className="absolute left-0 top-[-25px]" src={imgGradientLock} />
          <div className="home-image-box container-page lg:pr-32">
            <div className="home-border p-10">
              <div className="home-title-feature color-linear">
                sBTC: Unlocking Bitcoin by Satochain
              </div>
              <div className="flex flex-col gap-y-3 home-title-description">
                <div className="flex items-center gap-x-2 ">
                  <img src={iconCheck} className="text-base" />
                  Trust-minimized
                </div>
                <div className="flex items-center gap-x-2">
                  <img src={iconCheck} className="text-base" />
                  Decentralized
                </div>
                <div className="flex items-center gap-x-2">
                  <img src={iconCheck} className="text-base" />
                  Censorship resistant
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="flex items-center lg:flex-row lg:items-start lg:justify-evenly relative flex-col-reverse">
          <img className="absolute right-0 top-[-500px]" src={imgGradientBag} />
          <div className="home-image-box container-page lg:pl-32">
            <div className="home-border p-10">
              <div className="home-title-feature color-linear">
                Earn BTC by participating as validators
              </div>
              <div className="home-title-description">
                Hold and temporarily secure BTC, Satochain's native currency, to
                bolster the network's security and consensus. Over 2000 bitcoins
                have been allocated as consensus rewards
              </div>
              <CommonButton
                onClick={goToMintKeyPage}
                text="Mint your key now"
                className="mt-10 border-primary border w-fit text-primary"
              />
            </div>
          </div>
          <img src={imgBag} className="home-image-feature" />
        </div>
        <div className="flex flex-col items-center lg:flex-row lg:items-start lg:justify-evenly relative">
          <img
            className="absolute left-[-100px] top-[-90px]"
            src={imgGradientComputer}
          />
          <img src={imgComputer} className="home-image-feature" />
          <div className="home-image-box container-page lg:pr-32 ">
            <div className="home-border p-10">
              <div className="home-title-feature color-linear">
                Build powerful apps, secured by Bitcoin
              </div>
              <div className="home-title-description">
                Transactions on the Satochain layer are automatically finalized
                on Bitcoin, allowing for the creation of apps and digital assets
                that are integrated with the robust security of Bitcoin.
              </div>
              <CommonButton
                onClick={goToMintKeyPage}
                text="Start Building"
                className="mt-10 border-primary border w-fit text-primary"
              />
            </div>
          </div>
        </div>
      </div>
      <div className="container-page ">
        <Footer className="mt-[145px]" />
      </div>
    </div>
  );
}
