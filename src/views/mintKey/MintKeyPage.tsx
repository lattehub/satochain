import CommonInput from "components/common/CommonInput/CommonInput";
import "./mintkey.css";
import icExclamationMark from "assets/icons/ic-exclamation-mark.png";
import CommonInputNumberWithPlusAndMinus from "components/common/CommonInputNumberWithPlusAndMinus/CommonInputNumberWithPlusAndMinus";
import CommonSelect from "components/common/CommonSelect/CommonSelect";
import icBitcoin from "assets/icons/ic-bitcoin.png";
import CommonButton from "components/common/CommonButton/CommonButton";
import icCopy from "assets/icons/ic-copy.svg";
import Footer2 from "layouts/Footer2/Footer2";
import imgGradientLeft from "assets/images/mintkey/img-gradient-left.svg";
import imgGradientRight from "assets/images/mintkey/img-gradient-right.png";
import { useState } from "react";
export default function MintKeyPage() {
  const [recipientAdd, setRecipientAdd] = useState("");
  const [referrerCode, setReferrerCode] = useState("");
  const [quantity, setQuantity] = useState(10);
  return (
    <div className="relative">
      <img
        src={imgGradientLeft}
        className="absolute left-0 z-10 top-[-140px]"
      />
      <img
        src={imgGradientRight}
        className="absolute right-0 z-10 top-[-100px]"
      />
      <div className="container-page w-full mx-auto xl:w-[1200px] flex-col gap-y-10 xl:flex-row text-white mt-12 flex gap-x-8">
        <div className="w-full xl:w-[687px]">
          <div className="p-8 text-[20px] sm:text-[35px] md:text-[46px] font-bold bg-linear border-[1px] border-b-0 border-[#48494B] mintkey-cell">
            Mint Your Keys
          </div>
          <div className="mintkey-cell">
            <div className="flex justify-between mb-2">
              <span className="text-[11px] bg-white/5 p-1 text-white font-medium rounded-md">
                Tier 0 - SOLD OUT
              </span>
              <span className="text-[11px] bg-white/5 p-1 text-white font-medium rounded-md">
                Tier 1 - PROCESSING
              </span>
            </div>
            <div className="w-full bg-gray-200 rounded-full h-2.5 dark:bg-gray-700">
              <div className="bg-[#FFB597] h-2.5 rounded-full w-[45%]"></div>
            </div>
          </div>
          <div className="flex items-center md:justify-between mintkey-cell flex-col md:flex-row">
            <div className="w-[294px]">
              <div className="flex items-center gap-x-2 justify-center md:justify-start">
                <span className="text-[18px] sm:text-[24px] font-medium">
                  Sato Entry Key
                </span>
                <img src={icExclamationMark} />
              </div>
              <div className="text-xs sm:text-sm text-white/70 mt-2 text-center md:text-left">
                The Key to asserting your eligibility for network challenges and
                incentives.
              </div>
            </div>
            <div className="flex items-center gap-x-2 mt-5 md:mt-0">
              <span className="text-[14px] font-normal">
                <CommonInputNumberWithPlusAndMinus
                  labelElement={
                    <div className="flex gap-x-2 mb-2">
                      <span className="uppercase text-white/70 text-center md:text-left w-full text-[12px] sm:text-[14px]">
                        Quantity
                      </span>
                      <img src={icExclamationMark} className="object-contain" />
                    </div>
                  }
                  value={quantity}
                  setValue={setQuantity}
                />
              </span>
            </div>
          </div>
          <div className="mintkey-cell">
            <div className="flex justify-between sm:flex-row flex-col gap-y-4 sm:gap-y-0">
              <div className="sm:w-[294px]">
                <div className="text-[18px] sm:text-[24px] font-medium ">
                  1 x Sato Entry Key
                </div>
                <div className="text-sm text-white/70 mt-2">
                  0.001 BTC per key
                </div>
              </div>
              <div className="flex items-center gap-x-2 ">
                <CommonSelect
                  options={[
                    {
                      label: "Ethereum",
                      value: 1,
                      icon: <img src={icBitcoin} />,
                    },
                    {
                      label: "Bitcoin",
                      value: 2,
                      icon: <img src={icBitcoin} />,
                    },
                    {
                      label: "Arbitrum",
                      value: 3,
                      icon: <img src={icBitcoin} />,
                    },
                  ]}
                />
              </div>
            </div>
            <div className="mt-5 flex items-center justify-between text-sm sm:text-xl font-medium">
              <span>Pay</span>
              <span>0.001 BTC</span>
            </div>
          </div>
          <div className="mintkey-cell flex items-center pt-10 gap-x-3 justify-between flex-col sm:flex-row gap-y-3 sm:gap-y-0">
            <CommonInput
              value={recipientAdd}
              setValue={setRecipientAdd}
              className="flex-1"
              label="Recipient Address"
            />
            <CommonInput
              value={referrerCode}
              setValue={setReferrerCode}
              className="flex-1"
              label="Referrer's Code"
            />
          </div>
          <div className="mintkey-cell flex items-center pt-10 gap-x-3">
            <CommonButton text="Confirm" className="bg-linear w-full" />
          </div>
        </div>
        <div className="w-full xl:w-[481px]">
          <div className="mintkey-cell h-1/4 text-sm sm:text-base">
            <div className="bg-white/10 rounded-[4px] flex items-center justify-between p-3">
              <span className="w-[75px] sm:w-fit">Key Balance</span>
              <span className="text-[#FBB813] text-base font-bold">
                1 Entry Key
              </span>
            </div>
            <div className="bg-white/10 rounded-[4px] flex items-center justify-between p-3 mt-3 text-sm sm:text-base">
              <span className="w-[75px] sm:w-fit">My Referral Code (5%): </span>
              <div className="flex items-center gap-x-1 sm:gap-x-3">
                <span>8235a34374</span>
                <img src={icCopy} className="w-[12px] sm:w-[20px]" />
              </div>
            </div>
          </div>
          <div className="mintkey-cell h-1/4 flex items-center justify-between text-sm sm:text-base">
            <span>Gas Sharing Reward</span>
            <span>0.137 sBTC</span>
          </div>
          <div className="mintkey-cell h-1/4 flex items-center justify-between text-sm sm:text-base">
            <span>Key Incentive Rewards</span>
            <span>394,600 SATO</span>
          </div>
          <div className="mintkey-cell h-1/4 flex items-center justify-between text-sm sm:text-base">
            <span>Key Farm Rewards</span>
            <span>835 SATO</span>
          </div>
        </div>
      </div>
      <div className="mt-[110px]">
        <Footer2 />
      </div>
    </div>
  );
}
