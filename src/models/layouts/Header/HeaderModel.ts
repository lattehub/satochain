export interface Navigation {
  text: string;
  routerLink?: string;
  isHot?: boolean;
}
